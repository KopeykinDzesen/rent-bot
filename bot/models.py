from django.db import models


class Customer(models.Model):
    id = models.IntegerField(primary_key=True, unique=True)
    first_name = models.CharField(max_length=64, null=True)
    last_name = models.CharField(max_length=64, null=True)
    username = models.CharField(max_length=64, null=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name} ({self.username})'


class Apartment(models.Model):
    RENT_TYPES = {
        'room': 'rent_type%5B%5D=room&',
        1: 'rent_type%5B%5D=1_room&',
        2: 'rent_type%5B%5D=2_rooms&',
        3: 'rent_type%5B%5D=3_rooms&',
        4: 'rent_type%5B%5D=4_rooms&rent_type%5B%5D=5_rooms&rent_type%5B%5D=6_rooms&'
    }

    METRO_LINES = {
        'не важно': '',
        'возле метро': 'metro%5B%5D=red_line&metro%5B%5D=blue_line',
        'красная ветка': 'metro%5B%5D=red_line',
        'синяя ветка': 'metro%5B%5D=blue_line'
    }

    def generate_link(self):
        if not self.is_full():
            raise AttributeError('Apartment link cannot be generated.')
        base_link = 'https://ak.api.onliner.by/search/apartments?{rent_type}{price}currency=usd&{owner}{metro}'
        rent_type = self.RENT_TYPES['room'] if self.is_room else ''
        rent_type += self.RENT_TYPES[self.number_of_rooms] if self.number_of_rooms != 0 else ''
        price = f'price%5Bmin%5D={self.price_min}&price%5Bmax%5D={self.price_max}&'
        owner = 'only_owner=true&' if self.is_owner else ""
        metro = self.METRO_LINES[self.metro_line]
        return base_link.format(rent_type=rent_type, price=price, owner=owner, metro=metro)

    alias = models.CharField(max_length=64, null=True, blank=True)
    is_search = models.BooleanField(default=False)
    is_room = models.BooleanField(default=False)
    is_apartment = models.BooleanField(default=False)
    number_of_rooms = models.IntegerField(default=0)
    price_min = models.IntegerField(default=50)
    price_max = models.IntegerField(default=7000)
    is_owner = models.BooleanField(default=False)
    metro_line = models.CharField(max_length=24, null=True, blank=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True, blank=True)
    link = models.CharField(max_length=255, null=True, blank=True)
    id_last_apartment = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.alias if self.alias else self.get_default_alias()

    def save(self, *args, **kwargs):
        is_new = not self.pk

        if not is_new and self.is_search:
            self.link = self.generate_link()
            kwargs.get('update_fields', []).append('link')

        super(Apartment, self).save(*args,  **kwargs)

    def get_the_same_apartment(self):
        return Apartment.objects.filter(is_room=self.is_room,
                                        is_apartment=self.is_apartment,
                                        number_of_rooms=self.number_of_rooms,
                                        price_min=self.price_min,
                                        price_max=self.price_max,
                                        is_owner=self.is_owner,
                                        metro_line=self.metro_line) \
            .exclude(id=self.id)

    def get_default_alias(self):
        if not self.is_full():
            return str(self.id)
        return f'{"комната" if self.is_room else "квартира(" + str(self.number_of_rooms) + ")"}' \
            f' {self.price_min} - {self.price_max}$'

    def is_full(self):
        return all(((self.is_room or self.is_apartment),
                    (self.number_of_rooms != 0 and self.is_apartment) or self.is_room,
                    self.metro_line))
