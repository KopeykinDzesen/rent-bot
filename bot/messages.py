import re

from os import listdir
from os.path import join
from random import randint
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

from django.template import Template, Context
from django.template.loader import TemplateDoesNotExist

from .kinds import MessageKinds
from .models import Customer, Apartment


class BaseMessage:
    kind = MessageKinds.WTF
    base_path = 'bot/messages/'
    keyboard = None

    def __init__(self):
        template = Template(self.get_template())
        context = self.get_context()
        self.text = template.render(context)

    def get_template(self):
        path_to_dir = self.base_path + self.kind
        all_templates = [join(path_to_dir, f) for f in listdir(path_to_dir)]
        if not all_templates:
            raise TemplateDoesNotExist(f'kind: {self.kind}')
        path_to_template = all_templates[randint(0, len(all_templates) - 1)]

        with open(path_to_template, 'r') as file:
            return str(file.read())

    def get_context(self):
        return Context()


class MessageAnyMessage(BaseMessage):
    kind = MessageKinds.ANY_MESSAGE

    def __init__(self, customer, text):
        self.text = text
        self.customer = customer

        if self.is_hello_message:
            self.kind = MessageKinds.HELLO

        super(MessageAnyMessage, self).__init__()

    @property
    def is_hello_message(self):
        return bool(re.search(r'^(Hello|hello|hi)[,\.\ ]+(World|world|wrld)[!|\.]*$', self.text))

    def get_context(self):
        if self.is_hello_message:
            return MessageHelloCommand(self.customer).get_context()
        return Context()


class MessageStartCommand(BaseMessage):
    kind = MessageKinds.START

    def __init__(self, customer):
        self.customer_name = customer.first_name
        super(MessageStartCommand, self).__init__()

    def get_context(self):
        return Context({'customer_name': self.customer_name})


class MessageHelpCommand(BaseMessage):
    kind = MessageKinds.HELP


class MessageHelloCommand(BaseMessage):
    kind = MessageKinds.HELLO

    def __init__(self, customer):
        self.customer_name = customer.first_name
        self.customer_username = customer.username
        super(MessageHelloCommand, self).__init__()

    def get_context(self):
        return Context({
            'customer_name': self.customer_name,
            'customer_username': self.customer_username
        })


class MessageAllCommandsCommand(BaseMessage):
    kind = MessageKinds.ALL_COMMANDS


class MessageNewApartment(BaseMessage):
    kind = MessageKinds.NEW_APARTMENT


class MessageWhatDoYouSearch(BaseMessage):
    kind = MessageKinds.WHAT_DO_YOU_SEARCH

    def __init__(self, apartment_id, is_settings=False):
        callback = 'new_apartment:rent_type:{}:' + str(apartment_id)
        if is_settings:
            callback = 'settings:rent_type:{}:' + str(apartment_id)

        self.keyboard = InlineKeyboardMarkup(inline_keyboard=[
            [InlineKeyboardButton(text='квартира', callback_data=callback.format('apartment')),
             InlineKeyboardButton(text='комната', callback_data=callback.format('room'))],
            [InlineKeyboardButton(text='я передумал', callback_data='new_apartment:exit::' + str(apartment_id)
             if not is_settings else 'settings:edit:back:' + str(apartment_id))],
        ])

        super(MessageWhatDoYouSearch, self).__init__()


class MessageNumberOfRooms(BaseMessage):
    kind = MessageKinds.HOW_MANY_ROOMS

    def __init__(self, apartment_id, is_settings=False):
        callback = 'new_apartment:number_of_rooms:{}:' + str(apartment_id)
        if is_settings:
            callback = 'settings:number_of_rooms:{}:' + str(apartment_id)

        self.keyboard = InlineKeyboardMarkup(inline_keyboard=[
            [InlineKeyboardButton(text='1', callback_data=callback.format('1')),
             InlineKeyboardButton(text='2', callback_data=callback.format('2'))],
            [InlineKeyboardButton(text='3', callback_data=callback.format('3')),
             InlineKeyboardButton(text='4+', callback_data=callback.format('4'))],
            [InlineKeyboardButton(text='я передумал', callback_data='new_apartment:exit::' + str(apartment_id)
                                  if not is_settings else 'settings:edit:back:' + str(apartment_id))],
        ])

        super(MessageNumberOfRooms, self).__init__()


class MessageMinPrice(BaseMessage):
    kind = MessageKinds.WHAT_IS_MIN_PRICE
    MIN_PRICES = (50, 100, 150, 200, 250)

    def __init__(self, apartment_id, is_settings=False):
        callback = 'new_apartment:price_min:{}:' + str(apartment_id)
        if is_settings:
            callback = 'settings:price_min:{}:' + str(apartment_id)

        self.keyboard = InlineKeyboardMarkup(inline_keyboard=[
            [InlineKeyboardButton(text=str(price),
                                  callback_data=callback.format(price))
             for price in self.MIN_PRICES],
            [InlineKeyboardButton(text='без ограничений', callback_data=callback.format('50'))],
            [InlineKeyboardButton(text='я передумал', callback_data='new_apartment:exit::' + str(apartment_id)
                                  if not is_settings else 'settings:edit:back:' + str(apartment_id))],
        ])

        super(MessageMinPrice, self).__init__()


class MessageMaxPrice(BaseMessage):
    kind = MessageKinds.WHAT_IS_MAX_PRICE
    MAX_PRICES = (200, 250, 300, 400, 500)

    def __init__(self, apartment_id, price_min, is_settings=False):
        callback = 'new_apartment:price_max:{}:' + str(apartment_id)
        if is_settings:
            callback = 'settings:price_max:{}:' + str(apartment_id)

        self.price_min = price_min
        max_prices = tuple(filter(lambda price: price > self.price_min, self.MAX_PRICES))
        self.keyboard = InlineKeyboardMarkup(inline_keyboard=[
            [InlineKeyboardButton(text=str(price),
                                  callback_data=callback.format(price))
             for price in max_prices],
            [InlineKeyboardButton(text='без ограничений', callback_data=callback.format('7000'))],
            [InlineKeyboardButton(text='я передумал', callback_data='new_apartment:exit::' + str(apartment_id)
                                  if not is_settings else 'settings:edit:back:' + str(apartment_id))],
        ])

        super(MessageMaxPrice, self).__init__()


class MessageIsOwner(BaseMessage):
    kind = MessageKinds.WHAT_IS_OWNER

    def __init__(self, apartment_id, is_settings=False):
        callback = 'new_apartment:is_owner:{}:' + str(apartment_id)
        if is_settings:
            callback = 'settings:is_owner:{}:' + str(apartment_id)

        self.keyboard = InlineKeyboardMarkup(inline_keyboard=[
            [InlineKeyboardButton(text='только собственник', callback_data=callback.format('owner'))],
            [InlineKeyboardButton(text='можно и агенство', callback_data=callback.format('agent'))],
            [InlineKeyboardButton(text='я передумал', callback_data='new_apartment:exit::' + str(apartment_id)
                                  if not is_settings else 'settings:edit:back:' + str(apartment_id))],
        ])

        super(MessageIsOwner, self).__init__()


class MessageMetroLine(BaseMessage):
    kind = MessageKinds.WHICH_SUBWAY
    METRO_LINES = ('не важно', 'возле метро', 'красная ветка', 'синяя ветка')

    def __init__(self, apartment_id, is_settings=False):
        callback = 'new_apartment:metro_line:{}:' + str(apartment_id)
        if is_settings:
            callback = 'settings:metro_line:{}:' + str(apartment_id)

        inline_keyboard = [
            [InlineKeyboardButton(text=line, callback_data=callback.format(line))] for line in self.METRO_LINES]
        inline_keyboard.append(
            [InlineKeyboardButton(text='я передумал', callback_data='new_apartment:exit::' + str(apartment_id)
                                  if not is_settings else 'settings:edit:back:' + str(apartment_id))],)
        self.keyboard = InlineKeyboardMarkup(inline_keyboard=inline_keyboard)

        super(MessageMetroLine, self).__init__()


class MessageConfirmationApartment(BaseMessage):
    kind = MessageKinds.CONFIRM_APARTMENT

    def __init__(self, apartment):
        callback = 'new_apartment:confirm:{}:' + str(apartment.id)
        self.apartment = apartment
        self.keyboard = InlineKeyboardMarkup(inline_keyboard=[
            [InlineKeyboardButton(text='да, начинай искать!', callback_data=callback.format('yes')),
             InlineKeyboardButton(text='нет, забудь всё что я сказал', callback_data=callback.format('no'))],
        ])

        super(MessageConfirmationApartment, self).__init__()

    def get_context(self):
        return Context({'apartment': self.apartment})


class MessageSuccessCreateApartment(BaseMessage):
    kind = MessageKinds.SUCCESS_CREATE_APARTMENT


class MessageExitFromPoll(BaseMessage):
    kind = MessageKinds.EXIT_FROM_APARTMENT_POLL


class MessageSettingsStart(BaseMessage):
    kind = MessageKinds.SETTINGS_START

    def __init__(self, customer_id):
        callback = 'settings:select_apartment:id:{}'
        self.apartments = Customer.objects.get(id=customer_id).apartment_set.all().order_by('id')
        inline_keyboard = [
            [InlineKeyboardButton(text=str(apartment[1]) + '  #' + str(apartment[0] + 1),
                                  callback_data=callback.format(apartment[1].id))]
            for apartment in enumerate(self.apartments)
        ]
        inline_keyboard.append([InlineKeyboardButton(text='выйти', callback_data='settings:exit::')])
        self.keyboard = InlineKeyboardMarkup(inline_keyboard=inline_keyboard)

        super(MessageSettingsStart, self).__init__()

    def get_context(self):
        return Context({'count_apartrments': len(self.apartments)})


class MessageSettingsSelect(BaseMessage):
    kind = MessageKinds.SETTINGS_SELECT
    CHANGES = (
        ('rename', 'переименовать'),
        ('edit', 'редактировать'),
        ('delete', 'удалить')
    )

    def __init__(self, apartment_id, change=None):
        self.change = change
        self.apartment = Apartment.objects.get(id=apartment_id)

        callback = 'settings:select_change:{}:' + str(apartment_id)
        inline_keyboard = [[InlineKeyboardButton(text=change[1], callback_data=callback.format(change[0]))]
                           for change in self.CHANGES]
        if self.apartment.is_search:
            inline_keyboard.append([InlineKeyboardButton(text='остановить поиск',
                                                         callback_data=callback.format('stop_search'))])
        else:
            inline_keyboard.append([InlineKeyboardButton(text='возобновить поиск',
                                                         callback_data=callback.format('start_search'))])
        inline_keyboard.append([InlineKeyboardButton(text='назад',
                                                     callback_data='settings:select_apartment:back:' + str(apartment_id))])
        inline_keyboard.append([InlineKeyboardButton(text='выйти', callback_data='settings:exit::')])
        self.keyboard = InlineKeyboardMarkup(inline_keyboard=inline_keyboard)

        super(MessageSettingsSelect, self).__init__()

    def get_context(self):
        return Context({'apartment': self.apartment, 'change': self.change})


class MessageSettingsRename(BaseMessage):
    kind = MessageKinds.SETTINGS_RENAME

    def __init__(self, apartment_id):
        self.apartment = Apartment.objects.get(id=apartment_id)
        self.keyboard = InlineKeyboardMarkup(inline_keyboard=[[InlineKeyboardButton(text='назад',
                                             callback_data='settings:select_change:back:' + str(apartment_id))]])

        super(MessageSettingsRename, self).__init__()

    def get_context(self):
        has_alias = bool(self.apartment.alias)
        return Context({'has_alias': has_alias, 'alias': str(self.apartment)})


class MessageSettingsEdit(BaseMessage):
    kind = MessageKinds.SETTINGS_EDIT

    EDIT_CHOICES = (
        ('rent_type', 'тип жилья'),
        ('number_of_rooms', 'кол-во комнат'),
        ('price_min', 'мин. цена'),
        ('price_max', 'макс. цена'),
        ('is_owner', 'агенство/собственник'),
        ('metro_line', 'метро')
    )

    def __init__(self, apartment, change=None):
        self.change = change
        self.apartment = apartment

        callback = 'settings:edit:{}:' + str(self.apartment.id)
        inline_keyboard = [[InlineKeyboardButton(text=choice[1], callback_data=callback.format(choice[0]))]
                           for choice in self.EDIT_CHOICES
                           if (choice[0] != 'number_of_rooms' and self.apartment.is_room)
                           or self.apartment.is_apartment]
        inline_keyboard.append([InlineKeyboardButton(text='назад',
                                                     callback_data='settings:rename:back:' + str(self.apartment.id))])
        self.keyboard = InlineKeyboardMarkup(inline_keyboard=inline_keyboard)

        super(MessageSettingsEdit, self).__init__()

    def get_context(self):
        return Context({'apartment': self.apartment,
                        'change': self.change})


class MessageSettingsDelete(BaseMessage):
    kind = MessageKinds.SETTINGS_DELETE

    def __init__(self, apartment_id):
        self.apartment = Apartment.objects.get(id=apartment_id)
        callback = 'settings:delete:{}:' + str(apartment_id)
        inline_keyboard = [[InlineKeyboardButton(text='да!', callback_data=callback.format('yes')),
                            InlineKeyboardButton(text='назад',
                                                 callback_data='settings:select_change:back:' + str(apartment_id))]]
        self.keyboard = InlineKeyboardMarkup(inline_keyboard=inline_keyboard)

        super(MessageSettingsDelete, self).__init__()

    def get_context(self):
        return Context({'apartment': self.apartment})
