

class MessageKinds:

    # unexpected error
    WTF = 'errors/wtf'

    # any message
    ANY_MESSAGE = 'any_message'

    # answers to commands
    START = 'commands/start'
    HELLO = 'commands/hello'
    HELP = 'commands/help'
    ALL_COMMANDS = 'commands/all_commands'

    # set apartment dialog
    NEW_APARTMENT = 'apartment_poll/new_apartment'
    CONFIRM_APARTMENT = 'apartment_poll/confirm_apartment'
    SUCCESS_CREATE_APARTMENT = 'apartment_poll/success_create_apartment'
    EXIT_FROM_APARTMENT_POLL = 'apartment_poll/exit_message'
    WHAT_DO_YOU_SEARCH = 'apartment_poll/what_do_you_search'
    HOW_MANY_ROOMS = 'apartment_poll/how_many_rooms'
    WHAT_IS_MIN_PRICE = 'apartment_poll/what_is_min_price'
    WHAT_IS_MAX_PRICE = 'apartment_poll/what_is_max_price'
    WHAT_IS_OWNER = 'apartment_poll/what_is_owner'
    WHICH_SUBWAY = 'apartment_poll/which_subway'

    # settings dialog
    SETTINGS_START = 'settings/start'
    SETTINGS_SELECT = 'settings/select'
    SETTINGS_RENAME = 'settings/rename'
    SETTINGS_EDIT = 'settings/edit'
    SETTINGS_DELETE = 'settings/delete'
