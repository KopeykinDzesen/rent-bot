import os
import telepot

from .messages import *
from .utils import delete_message

bot = telepot.Bot(os.environ.get('BOT_TOKEN'))
last_callback = None


class MessageHandler:
    def __init__(self, message):
        self.user_id = message['from']['id']
        self.first_name = message['from']['first_name']
        self.last_name = message['from'].get('last_name', '')
        self.username = message['from'].get('username', '')
        self.chat_id = message['chat']['id']
        self.message_id = message['message_id']

        self.text = message['text']

    @property
    def is_command(self):
        return re.search(r'^/\w+$', self.text)

    def handler(self):
        customer, created = Customer.objects.update_or_create(id=self.user_id, defaults={
            'first_name': self.first_name,
            'last_name': self.last_name,
            'username': self.username
        }
                                                              )

        if created:
            bot.sendMessage(customer.id, MessageStartCommand(customer).text)

        global last_callback
        if last_callback is not None:
            if last_callback.type_button == 'select_change' and last_callback.button == 'rename':
                apartment = Apartment.objects.get(id=last_callback.data)
                apartment.alias = self.text
                apartment.save(update_fields=['alias'])

                delete_message(self.chat_id, last_callback.message_id + 1)
                delete_message(self.chat_id, self.message_id)

                message = MessageSettingsSelect(last_callback.data, change='rename')
                bot.sendMessage(self.user_id, message.text, reply_markup=message.keyboard)

            last_callback = None

        elif not self.is_command:
            bot.sendMessage(customer.id, MessageAnyMessage(customer, self.text).text)

        elif self.text == '/start':
            bot.sendMessage(customer.id, MessageStartCommand(customer).text)

        elif self.text == '/help':
            bot.sendMessage(customer.id, MessageHelpCommand().text)

        elif self.text == '/hello':
            bot.sendMessage(customer.id, MessageHelloCommand(customer).text)

        elif self.text == '/all_commands':
            bot.sendMessage(customer.id, MessageAllCommandsCommand().text)

        elif self.text == '/new_apartment':
            bot.sendMessage(customer.id, MessageNewApartment().text)

            apartment = Apartment()
            apartment.save()

            message = MessageWhatDoYouSearch(apartment.id)
            bot.sendMessage(self.user_id, message.text, reply_markup=message.keyboard)

        elif self.text == '/settings':
            message = MessageSettingsStart(customer.id)
            bot.sendMessage(customer.id, message.text, reply_markup=message.keyboard)

        else:
            bot.sendMessage(customer.id, BaseMessage().text)


class Callback:

    def __init__(self, callback, update_id):
        self.user_id = callback['from']['id']
        self.update_id = update_id
        self.message_id = callback['message']['message_id']
        self.chat_id = callback['message']['chat']['id']

        callback_data = re.search(
            r'^(?P<type_handler>[\w|\s]*):(?P<type_button>[\w|\s]*):(?P<button>[\w|\s]*):(?P<data>[\w|\s]*)$',
            callback['data'])
        self.type_handler = callback_data['type_handler']
        self.type_button = callback_data['type_button']
        self.button = callback_data['button']
        self.data = callback_data['data']

    def process_collback(self):
        if self.type_handler == 'new_apartment':
            NewApartmentHandler().handler(self.type_button, self.button, self.data,
                                          self.chat_id, self.message_id, self.user_id)

        if self.type_handler == 'settings':
            SettingsApartmentHandler().handler(self.type_button, self.button, self.data,
                                               self.chat_id, self.message_id, self.user_id)


class LastCallback(Callback):

    def __init__(self, type_handler, type_button, button, data, message_id):
        self.message_id = message_id
        self.type_handler = type_handler
        self.type_button = type_button
        self.button = button
        self.data = data


class NewApartmentHandler:

    @staticmethod
    def handler(type_button, button, data, chat_id, message_id, user_id):

        def save_current_state(aprt, ct_id, msg_id, update_fields=None):
            update_fields = update_fields or []
            aprt.save(update_fields=update_fields)
            delete_message(ct_id, msg_id)

        apartment = Apartment.objects.get(id=data)
        data_for_save = {'aprt': apartment, 'ct_id': chat_id, 'msg_id': message_id}

        if type_button == 'rent_type':
            if button == 'room':
                apartment.is_room = True
                save_current_state(**data_for_save, update_fields=['is_room'])
                message = MessageMinPrice(apartment.id)

            elif button == 'apartment':
                apartment.is_apartment = True
                save_current_state(**data_for_save, update_fields=['is_apartment'])
                message = MessageNumberOfRooms(apartment.id)

        elif type_button == 'number_of_rooms':
            apartment.number_of_rooms = int(button)
            save_current_state(**data_for_save, update_fields=['number_of_rooms'])
            message = MessageMinPrice(apartment.id)

        elif type_button == 'price_min':
            apartment.price_min = int(button)
            save_current_state(**data_for_save, update_fields=['price_min'])
            message = MessageMaxPrice(apartment.id, apartment.price_min)

        elif type_button == 'price_max':
            apartment.price_max = int(button)
            save_current_state(**data_for_save, update_fields=['price_max'])
            message = MessageIsOwner(apartment.id)

        elif type_button == 'is_owner':
            apartment.is_owner = button == 'owner'
            save_current_state(**data_for_save, update_fields=['is_owner'])
            message = MessageMetroLine(apartment.id)

        elif type_button == 'metro_line':
            apartment.metro_line = button
            save_current_state(**data_for_save, update_fields=['metro_line'])
            message = MessageConfirmationApartment(apartment)

        elif type_button == 'confirm' and not button == 'no':
            customer = Customer.objects.get(id=user_id)
            apartment.customer = customer
            apartment.is_search = True
            save_current_state(**data_for_save, update_fields=['customer', 'is_search'])
            bot.sendMessage(customer.id, MessageSuccessCreateApartment().text)
            return

        elif type_button == 'exit' or (type_button == 'confirm' and button == 'no'):
            apartment.delete()
            bot.deleteMessage((chat_id, message_id))
            bot.sendMessage(user_id, MessageExitFromPoll().text)
            return

        else:
            bot.sendMessage(user_id, BaseMessage().text)
            return

        bot.sendMessage(user_id, message.text, reply_markup=message.keyboard)


class SettingsApartmentHandler:

    @staticmethod
    def handler(type_button, button, data, chat_id, message_id, user_id):
        global last_callback

        if type_button == 'exit':
            delete_message(chat_id, message_id)
            last_callback = None
            return

        elif button == 'back':
            if type_button == 'select_apartment':
                message = MessageSettingsStart(user_id)
            elif type_button == 'select_change':
                message = MessageSettingsSelect(data)
            elif type_button == 'edit':
                apartment = Apartment.objects.get(id=data)
                message = MessageSettingsEdit(apartment)

        elif type_button == 'select_apartment':
            message = MessageSettingsSelect(data)

        elif type_button == 'select_change':

            if button == 'rename':
                message = MessageSettingsRename(data)
                last_callback = LastCallback('settings', 'select_change', 'rename', data, message_id)

            elif button == 'edit':
                apartment = Apartment.objects.get(id=data)
                message = MessageSettingsEdit(apartment)

            elif button == 'delete':
                message = MessageSettingsDelete(data)

            elif button == 'stop_search':
                apartment = Apartment.objects.get(id=data)
                apartment.is_search = False
                apartment.save(update_fields=['is_search'])
                message = MessageSettingsSelect(data, change='stop_search')

            elif button == 'start_search':
                apartment = Apartment.objects.get(id=data)
                apartment.is_search = True
                apartment.save(update_fields=['is_search'])
                message = MessageSettingsSelect(data, change='start_search')

        elif type_button == 'edit':

            if button == 'rent_type':
                message = MessageWhatDoYouSearch(data, is_settings=True)

            elif button == 'number_of_rooms':
                message = MessageNumberOfRooms(data, is_settings=True)

            elif button == 'price_min':
                message = MessageMinPrice(data, is_settings=True)

            elif button == 'price_max':
                apartment = Apartment.objects.get(id=data)
                message = MessageMaxPrice(apartment.id, apartment.price_min, is_settings=True)

            elif button == 'is_owner':
                message = MessageIsOwner(data, is_settings=True)

            elif button == 'metro_line':
                message = MessageMetroLine(data, is_settings=True)

        elif type_button == 'rent_type':
            apartment = Apartment.objects.get(id=data)

            if button == 'room':
                apartment.is_room = True
                apartment.is_apartment = False
                apartment.save(update_fields=['is_room', 'is_apartment'])
                message = MessageSettingsEdit(apartment, change='rent_type')

            elif button == 'apartment':
                apartment.is_apartment = True
                apartment.is_room = False
                apartment.number_of_rooms = 1
                apartment.save()
                message = MessageSettingsEdit(apartment, change='rent_type')

        elif type_button == 'number_of_rooms':
            apartment = Apartment.objects.get(id=data)
            apartment.number_of_rooms = int(button)
            apartment.save(update_fields=['number_of_rooms'])
            message = MessageSettingsEdit(apartment, change='number_of_rooms')

        elif type_button == 'price_min':
            apartment = Apartment.objects.get(id=data)
            apartment.price_min = int(button)
            apartment.save(update_fields=['price_min'])
            message = MessageSettingsEdit(apartment, change='price_min')

        elif type_button == 'price_max':
            apartment = Apartment.objects.get(id=data)
            apartment.price_max = int(button)
            apartment.save(update_fields=['price_max'])
            message = MessageSettingsEdit(apartment, change='price_max')

        elif type_button == 'is_owner':
            apartment = Apartment.objects.get(id=data)
            apartment.is_owner = button == 'owner'
            apartment.save(update_fields=['is_owner'])
            message = MessageSettingsEdit(apartment, change='is_owner')

        elif type_button == 'metro_line':
            apartment = Apartment.objects.get(id=data)
            apartment.metro_line = button
            apartment.save(update_fields=['metro_line'])
            message = MessageSettingsEdit(apartment, change='metro_line')

        elif type_button == 'delete' and button == 'yes':
            apartment = Apartment.objects.get(id=data)
            apartment.delete()
            message = MessageSettingsStart(user_id)

        delete_message(chat_id, message_id)
        bot.sendMessage(user_id, message.text, reply_markup=message.keyboard)
