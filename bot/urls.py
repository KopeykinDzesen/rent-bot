from django.urls import path

from .views import BotHandler


urlpatterns = [
    path('hook', BotHandler.as_view(), name='hook'),
]
