from __future__ import absolute_import, unicode_literals

import os

from celery import task
from requests import request

import telepot

from .models import Apartment


@task
def get_new_apartments():
    bot = telepot.Bot(os.environ.get('BOT_TOKEN'))
    apartments = Apartment.objects.filter(is_search=True)
    print(apartments)

    for apartment in apartments:
        response = request('GET', apartment.link)

        try:
            new_apartment = response.json().get('apartments', [])[0]
        except IndexError:
            continue

        if new_apartment['id'] != apartment.id_last_apartment:
            try:
                bot.sendMessage(apartment.customer.id, new_apartment['url'])
                apartment.id_last_apartment = new_apartment['id']
                apartment.save(update_fields=['id_last_apartment'])
            except telepot.exception.TelegramError:
                pass
