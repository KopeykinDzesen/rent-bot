import os
import telepot


bot = telepot.Bot(os.environ.get('BOT_TOKEN'))


def delete_message(chat_id, message_id):
    try:
        bot.deleteMessage((chat_id, message_id))
    except telepot.exception.TelegramError:
        '''Sometimes telepot delete one message two times'''
        pass
