import json

from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.http.response import HttpResponse

from .handlers import MessageHandler, Callback


class BotHandler(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(BotHandler, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)

        from pprint import pprint
        pprint(data)

        if data.get('message', ''):
            MessageHandler(data['message']).handler()
        elif data.get('callback_query', ''):
            Callback(data['callback_query'], data['update_id']).process_collback()

        return HttpResponse('OK')
