import json
import os

import requests

tunnels = requests.get("http://127.0.0.1:4040/api/tunnels").content
tunnels = json.loads(tunnels)["tunnels"]
https_tunnels = list(
    filter(
        lambda tunnel: tunnel["proto"] == "https"
        and "ngrok.io" in tunnel["public_url"],
        tunnels,
    )
)

current_tunnel = https_tunnels[0]["public_url"]
request = f'https://api.telegram.org/bot{os.environ.get("BOT_TOKEN")}/setWebhook?url={current_tunnel}/hook'
print(request)

response = requests.get(request)
print(current_tunnel)
print(response)
